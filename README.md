## API FOR VSUALIZATION OF A DATABASE

This local interface shows the data that belongs to a database developed in postgres on Linux mint

#### EVIDENCE

![eye_browser](/uploads/d40b9915216fed5d6c7fe71d52db5f3d/eye_browser.png)
>API from the browser

##### OPTIONS
+ Only read.

![termial](/uploads/5686516c7789ad480c1422228e6aa760/termial.png)
>Inserting data from the terminal

#### PHP code
    <?php
	try{
		$myPDO = new PDO("pgsql:host=localhost;dbname=tabla","postgres","postgres");
		$sql = "SELECT * FROM compañeros";
		foreach($myPDO->query($sql)as $row){
			echo "<tr>";
          echo "<td>"; echo $row['nombre']; echo "</td>";
          echo "<td>"; echo $row['apellido']; echo "</td>";
          echo "<td>"; echo $row['edad']; echo "</td>";
          echo "<td>"; echo $row['telefono']; echo "</td>";
          echo "</try>";
		}		
		}catch(PDOException $e){
			echo $e->getMessage();		
		}
		
	?>

#### HTML code
```html
<!DOCTYPE html>
<html>
	<body bgcolor="e1e1e1">
		<div id="contenido">
		<br><br><br>
  	<table style="margin: auto; width: 800px; border-collapse: separate; border-spacing: 10px 5px;">
  		<thead>
  			<td><h3>NOMBRE</h3></td>
  			<td><h3>APELLIDO</h3></td>
  			<td><h3>EDAD</h3></td> 
  			<td><h3>TELEFONO</h3></td>
  		</thead>
  		
  		<?php
</table>
  </div>
</body>
```
